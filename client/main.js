var _addSelectOption = function (elemId, value, text) {
    var $sel = $(elemId);

    var newOption = document.createElement('option');
    newOption.value = value;
    newOption.innerText = text;

    $sel.append(newOption);
};

var _addTagCheckbox = function (value, text) {
    var tagName = 'selTag';

    var newLabel = document.createElement('label');
    newLabel.for = tagName;
    newLabel.innerText = text;

    var newInput = document.createElement('input');
    newInput.type = 'checkbox';
    newInput.name = tagName;
    newInput.value = value;
    $(newInput).change(function () {
        $('#results').html('');
        _getContentByTags();
    });

    var newTag = document.createElement('div');
    newTag.classList.add('tag');
    newTag.appendChild(newLabel);
    newTag.appendChild(newInput);

    $('[data-field="tags"]').append(newTag);
}

var _makeResultCard = function (data) {
    var $results = $('#results');

    var card = document.createElement('div');
    card.classList.add('card');

    var cardHeader = document.createElement('h3');
    cardHeader.innerText = data.title + ' (' + String(data.year) + ')';
    card.appendChild(cardHeader);

    var cardContent = document.createElement('p');
    cardContent.innerText = data.content;
    card.appendChild(cardContent);

    if (data.tags) {
        var cardTags = document.createElement('div');
        cardTags.classList.add('card-tags');

        var selectedTags = _getSelectedTags();

        data.tags.forEach(tag => {
            var cardTag = document.createElement('div');
            cardTag.classList.add('card-tag');
            cardTag.innerText = tag.name;

            if (selectedTags.indexOf(String(tag.id)) !== -1) {
                cardTag.classList.add('active');
            }

            cardTags.appendChild(cardTag);
        });

        card.appendChild(cardTags);
    }

    $results.append(card);
};

var _displayInput = function (selectedInput) {
    var inputWrappers = document.querySelectorAll('.input-wrapper');

    inputWrappers.forEach(function (input) {
        if (input.dataset.field === selectedInput) {
            input.classList.add('active');
        }
        else {
            input.classList.remove('active');
        }
    });
};

var _getSelectedTags = function () {
    var selectedTags = [] ;

    $('[name="selTag"]:checked').each(function () {
        selectedTags.push($(this).val());
    });

    return selectedTags;
}

var _getContent = function (selVal) {
    //--call the server to get the content for the selected element
    $.get('/getContent?id=' + selVal, function (data) {
        if (data.error) {//--if there was an error, let the user know
            alert(data.error);
        } else {//--create a card with the title and content from the server
            _makeResultCard(data);
        }
    });
};

var _getContentByYear = function (selVal) {
    //--call the server to get the content for the selected element
    $.get('/getContentByYear?year=' + selVal, function (data) {
        if (data.error) {//--if there was an error, let the user know
            alert(data.error);
        } else {//--create a card with the title and content from the server
            data.map(_makeResultCard);
        }
    });
};

var _getContentByTags = function () {
    var selectedTags = _getSelectedTags();

    $.get('/getContentByTags?tags=' + selectedTags.join(','), function (data) {
        if (data.error) {
            alert(data.error);
        }
        else {
            data.map(_makeResultCard);
        }
    });
};

$(document).ready(function () {
    //when the document has loaded, we need to ask the server for a list of names and then populate the select list control
    $.get('/getNames', function (data) {
        //this code is run only once - when the document loads
        //data is an array of objects containing an id and value - we loop through these and add them to the select list
        $.each(data, function (i, nameObj) {//--this is the jQuery way of writing a for each loop
            _addSelectOption('#selName', nameObj.id, nameObj.value);
        });
    });

    $.get('/getYears', function (data) {
        $.each(data, function(_, yearObj) {
            _addSelectOption('#selYear', yearObj.value, yearObj.value);
        });
    });

    $.get('/getTags', function (data) {
        $.each(data, function(_, tagObj) {
            _addTagCheckbox(tagObj.id, tagObj.value);
        });
    });

    var $selInput = $('input[name="selInput"]');
    $selInput.change(function () {
        $("#results").html("");
        _displayInput($(this).val());
    });
    _displayInput($selInput.val());

    var $selName = $('#selName');//store a reference to the HTML element whose ID is 'selName';
    $selName.change(function () {
        $('#results').html("");

        //run this code whenever the user picks a new item from the select list
        var selVal = $(this).val();//get the current value for "selName" - the 'this' keyword can be confusing in javascript, but within a jQuery callback it usually refers to the object that has been clicked or selected
        _getContent(selVal);
    });

    var $selYear = $('#selYear');
    $selYear.change(function () {
        $('#results').html("");

        var selVal = $(this).val();
        _getContentByYear(selVal);
    });
});
