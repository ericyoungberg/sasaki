//region node.js core
var url = require('url');
//endregion
//region npm modules
var mysql = require('mysql');
//endregion
//region modules

//endregion

/**
 @class DataHandler
 */
var DataHandler = function () {
    var _self = this;
    var _connectionInfo = {
        host: process.env.DB_HOST,
        user: process.env.C9_USER,
        password: '',
        database: 'c9'
    };
    //region private fields and methods
    var _runQuery = function (query, callback) {
        var connection = mysql.createConnection(_connectionInfo);
        connection.connect();
        connection.query(query, function (err, rows, fields) {
            if (err) throw err;
            if (callback) callback(rows);
        });

        connection.end();
    };

    var _returnJsonObj = function (res, obj) {
        //use the standard response object to return data to the client as JSON.
        res.writeHeader(200, {"Content-Type": "application/json"});
        res.write(JSON.stringify(obj));
        res.end();
    };

    var _parseUrlGetVars = function (req) {//--parse the GET variables part of the URL
        var url_parts = url.parse(req.url, true);
        return url_parts.query;
    };
    //endregion

    //region public API

    this.getNames = function (req, res) {
        //--call our query function and pass in a callback function to use when it successfully gets rows for the query
        _runQuery('SELECT `UID`, `ProjectName` FROM `projects`', function (rows) {
            var projects = [];
            rows.forEach(function (row) {//each row object contains the SQL field names as properties
                projects.push({id: row.UID, value: row.ProjectName});//we make an array of new objects with an id and value to return to the client
            });
            _returnJsonObj(res, projects);
        });
    };

    this.getYears = function (req, res) {
        //--call our query function and pass in a callback function to use when it successfully gets rows for the query
        _runQuery('SELECT DISTINCT `ProjectYear` FROM `projects`', function (rows) {
            var projects = [];
            rows.forEach(function (row) {//each row object contains the SQL field names as properties
                projects.push({value: row.ProjectYear});//we make an array of new objects with an id and value to return to the client
            });
            _returnJsonObj(res, projects);
        });
    };

    this.getTags = function (req, res) {
        //--call our query function and pass in a callback function to use when it successfully gets rows for the query
        _runQuery('SELECT `UID`, `Name` FROM `tags`', function (rows) {
            var tags = [];
            rows.forEach(function (row) {//each row object contains the SQL field names as properties
                tags.push({id: row.UID, value: row.Name});//we make an array of new objects with an id and value to return to the client
            });
            _returnJsonObj(res, tags);
        });
    };

    this.getContentById = function (req, res) {
        var id = _parseUrlGetVars(req).id;//get the id that we passed in from the client side as a GET variable: e.g. getContent?id=4
        _runQuery('SELECT `ProjectName`, `Description`, `ProjectYear` FROM `projects` WHERE `UID` = ' + id, function (rows) {
            if (rows.length == 0) {
                _returnJsonObj(res, {error: "No Results Found"});
            }
            if (rows.length > 1) {
                _returnJsonObj(res, {error: "Multiple results!"});//--should never happen with unique ID
            }
            var row = rows[0];
            _returnJsonObj(res, {title: row.ProjectName, content: row.Description, year: row.ProjectYear});
        });
    };

    this.getContentByYear = function (req, res) {
        var year = _parseUrlGetVars(req).year;//get the id that we passed in from the client side as a GET variable: e.g. getContent?id=4
        _runQuery('SELECT `ProjectName`, `Description`, `ProjectYear` FROM `projects` WHERE `ProjectYear` = ' + year, function (rows) {
            if (rows.length == 0) {
                _returnJsonObj(res, {error: "No Results Found"});
            }

            _returnJsonObj(res, rows.map(row => ({title: row.ProjectName, content: row.Description, year: row.ProjectYear})));
        });
    };

    this.getContentByTags = function (req, res) {
        // Lookup tags first to use as reference for building response payload
        _runQuery('SELECT `UID`, `Name` from `tags`', function (allTags) {
            // Create a dictionary to lookup tags in constant time
            var tagTable = allTags.reduce((table, tag) => {
                table[String(tag.UID)] = {id: tag.UID, name: tag.Name};
                return table;
            }, {});

            var tags = _parseUrlGetVars(req).tags;
            var whereClause = tags.split(',').map(tag => "`Tags` LIKE '%," + String(tag) + ",%'").join(' OR ');

            _runQuery('SELECT `ProjectName`, `Description`, `ProjectYear`, `Tags` FROM `projects` WHERE ' + whereClause, function (rows) {
                if (rows.length == 0) {
                    _returnJsonObj(res, {error: "No Results Found"});
                }

                rows.forEach(row => {
                    row.Tags = row.Tags.split(',').map(tag => tagTable[tag]).filter(tag => tag);
                });

                _returnJsonObj(res, rows.map(row => ({title: row.ProjectName, content: row.Description, year: row.ProjectYear, tags: row.Tags})));
            });
        });
    };
    //endregion
};

module.exports.DataHandler = DataHandler;

